# -*- coding: utf-8 -*-

import maya.cmds as cmds
import re
import numpy as np
from functools import partial
import os

class FileManager(object):


    def __init__(self):
        """Contructor, se especifican nombres de archivos"""
        # Especificación de los nombres de los archivo e la boca y la casa
        self.mouth_path = "mouth.mb"
        self.mouthName = ""
        self.face_path = "face.mb"

    def importTract(self, path):
        """Constructor """
        #obtencion de los objetos que hay antes de importar
        before = set(cmds.ls(assemblies=True))
        #importacion
        cmds.file(path , i=True, mergeNamespacesOnClash=True, namespace=':')
        
        #obtencion de los objetos que hay despues de importar
        after  =set(cmds.ls(assemblies=True))

        # Extraccion de los objetos de antes de exportar a kis objetos despues de exportar.
        # Para quedarse con los objetos importados 
        imported = after.difference(before)
        cmds.select(imported)
        return imported

    def importFile(self, object_name, file_name):
        """Impoerta un archivo y borra los archivos que no se llamen como object_Name"""
        #obtencion de los objetos que hay antes de importar
        before = set(cmds.ls(assemblies=True))
        #importo
        file_path = os.path.dirname(__file__) + "/" + file_name
        cmds.file(file_path , i=True, mergeNamespacesOnClash=True, namespace=':', str = False)
        
        #obtencion de los objetos que hay despues de importar
        after  =set(cmds.ls(assemblies=True))
        
        # Extraccion de los objetos de antes de exportar a kis objetos despues de exportar.
        # Para quedarse con los objetos importados 
        imported = after.difference(before)

        #Los objetos del archiivo que no sean la caraola boca se eliminan
        for imported_obj in imported:
            if imported_obj not in object_name:
                cmds.delete(imported_obj)
            else:
                self.mouthName = imported_obj

    def importFace(self):
        """Importa la cara"""
        self.full_face = True
        self.importFile("Head_targets_mb:head_base", self.face_path)

    def importMouth(self):
        """Importa la boca"""
        self.full_face = False
        self.importFile("BocaFBXASC032Main", self.mouth_path)

    def getMouthName(self):
        """Devuelve el nombre de la boca o cara """
        return self.mouthName

    def getFullFace(self):
        return self.full_face
