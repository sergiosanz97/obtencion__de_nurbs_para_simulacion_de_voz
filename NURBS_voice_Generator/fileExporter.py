# -*- coding: utf-8 -*-

import maya.cmds as cmds
import re
import numpy as np
from functools import partial

class FileManager(object):


    def __init__(self):
        """Contructor, se especifican nombres de archivos"""
        # Especificación de los nombres de los archivo e la boca y la casa
        self.mouthName = ""

    def importTract(self, path):
        """Constructor """
        #obtencion de los objetos que hay antes de importar
        before = set(cmds.ls(assemblies=True))
        #importacion
        cmds.file(path , i=True, mergeNamespacesOnClash=True, namespace=':')
        
        #obtencion de los objetos que hay despues de importar
        after  =set(cmds.ls(assemblies=True))

        # Extraccion de los objetos de antes de exportar a kis objetos despues de exportar.
        # Para quedarse con los objetos importados 
        imported = after.difference(before)
        cmds.select(imported)
        return imported

    def getMouthName(self):
        """Devuelve el nombre de la boca o cara """
        return self.mouthName
