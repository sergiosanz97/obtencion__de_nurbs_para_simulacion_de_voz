# -*- coding: utf-8 -*-

import maya.cmds as cmds
import re
import numpy as np
import math
from functools import partial
import maya.mel as mel
import utils



class MorpherAdapter(object):

    #nombres de los morphers
    ai = "Morpher_ncl1_1.BocaFBXASC032AFBXASC044I"
    e ="Morpher_ncl1_1.MouthFBXASC032E"
    o = "Morpher_ncl1_1.MouthFBXASC032O"
    u = "Morpher_ncl1_1.MouthFBXASC032U"

    firstMorpher_modify = False


    def __init__(self):
        """Constructor"""
        self.firstMorpher_modify = False

    def getBocalTractCurveArea(self, name):
        """se obtiene el area de la superficie de la curva del borde de la boca
        mediante las dos mayores amplitudes de los ejes de la bounding box de la
         curva de la boca
         """

        # En el caso de que ya se haya calculado una vez NO 
        # es necesario volver a calcular el area ni saber los ejes 
        if self.firstMorpher_modify == True:
            return self.getLipsCurveArea(name)
        

        bb = cmds.exactWorldBoundingBox(name)

        self.x = False 
        self.y = False
        self.z = False
        bbs = []

        # Se buscan los dos ejes para calcular el area de la curva del extremo del labio y se calcula el area
        zero = 0.00001
        if abs(bb[0] - bb[3]) >= zero:
            self.x = True
            bbs.append(abs(bb[0] - bb[3])) 

        if abs(bb[1] - bb[4]) >= zero:
            self.y = True 
            bbs.append(abs(bb[1] - bb[4])) 

        if abs(bb[2] - bb[5]) >= zero:
            self.z =True 
            bbs.append(abs(bb[2] - bb[5])) 
        
        self.firstMorpher_modify = True
        return bbs[0], bbs[1]

    def setFaceName(self, name):
        """Set de la nombre de a malla de la cara/boca"""
        self.face_name = name

    def getLipsCurveArea(self, name):
        """Se calcula el area de la curva a partir de los ejes seleccionados en la funcion getBocalTractCurveArea()"""
        edges = []

        edge1 = cmds.exactWorldBoundingBox(name)

        if self.x == True:
            edges.append(abs(edge1[0] - edge1[3])) 

        if self.y == True:
            edges.append(abs(edge1[1] - edge1[4])) 

        if self.z == True:
            edges.append(abs(edge1[2] - edge1[5]))

        return edges[0], edges[1]

    def setToZeroMorpherValues(self):
        """Se ponen todos los valores de influeciade los morphers a 0"""
        cmds.setAttr( self.ai, 0)
        cmds.setAttr( self.e, 0)
        cmds.setAttr( self.o, 0)
        cmds.setAttr( self.u, 0)

    def getMorpherName(self, name):
        """Se devuelve el nombre del morpher real en funcion del morpher que se haya indicado"""
        if name == "AI":
            return self.ai

        elif name == "E":
            return self.e

        elif name == "O":
            return self.o

        elif name == "U":
            return self.u

    def gradDescMorpher(self, morpher_vocal, lips_border, bocal_tract_border ):
        """Se calcula valor optimo, del morpher para que el extremo de la boca y el extremo del tracto sean lo mas similares posibles"""
        # Obtencion del nombre del morpher que se indique
        morpher_name = self.getMorpherName(morpher_vocal)

        # El valor inicial del morpher seleccionado 
        # para ese valor sera 0.5
        cmds.setAttr( morpher_name, 0.5)

        # Se calcula el area de la curva del extremo del tracto
        tract_curve_width, tract_curve_height = self.getBocalTractCurveArea(bocal_tract_border)

        delta = 0.01
        morpher_value = 0.5
        advance = 0.001

        limit_iteration = 300
        zero = 0.005
        area_derived = 1
        # Implementacion del gradiente descendiente
        while abs(area_derived) > 0.005:
            # Se calcula el area con f(x)
            cmds.setAttr( morpher_name, morpher_value)
            cmds.refresh(f = True)
            lips_border_width, lips_border_height  = self.getLipsCurveArea(lips_border)
            pos =cmds.xform(lips_border + '.cv[0]', q=True, ws=True, t=True )


            # Se calcula el area con f(x + invremento_X)
            morpher_delta_value = morpher_value + delta
            cmds.setAttr( morpher_name,morpher_delta_value)
            cmds.refresh(f = True)
            lips_border_width_delta, lips_border_height_delta = self.getLipsCurveArea(lips_border)

            # Se calcula la derivada
            area_derived = (((lips_border_width_delta - tract_curve_width )** 2 + (lips_border_height_delta - tract_curve_height )** 2 ) - ((lips_border_width - tract_curve_width )** 2 + (lips_border_height - tract_curve_height )** 2 )) / delta

            # Se cambia el valor del morpher
            morpher_value = morpher_value - area_derived * advance

            # dado que el valor maxima del morpher 1 y el minimo 0, 
            # se asegura que el valor este en este rango
            if morpher_value > 1:
                morpher_value = 1
                break
            elif morpher_value < 0:
                morpher_value = 0
                break

        # Se asigna el valor final del morpher
        cmds.setAttr(morpher_name, morpher_value)

        # Polyclean para hacer correctamente la conversion a subdiv y nurbs
        cmds.select(self.face_name)
        mel.eval('polyCleanupArgList 4 { "0","1","1","0","0","0","0","0","0","1e-05","0","1e-05","0","1e-05","0","1","0","0" };')

    def alignLipsAndTract(self, tract_name, tract_border, lips_border, face_name, is_full_face):
        """Colocacion del tracto vocal"""
        # Reset el borde lips_border 
        cmds.xform(lips_border, cp = True, p = True)
        
        # Bake el borde de la cara
        cmds.select(lips_border)
        utils.bakeCustomToolPivot(pos=1, ori=0)
        
        # Reset el borde tract_border 
        cmds.xform(tract_border, cp = True, p = True)
        
        # Bake el borde del tracto
        cmds.select(tract_border)
        utils.bakeCustomToolPivot(pos=1, ori=0)


        # Posicion de la curva del borde
        lips_border_pos =cmds.xform(lips_border , q=True, ws=True, rp=True)

        pos1 = None
        pos2 = None
        if is_full_face == False:
            # posicion vertice superior del labio
            pos1 = cmds.xform(self.face_name + ".vtx[46]", q=True, ws=True, t=True)

            # posicion vertice inferior del labio 
            pos2 = cmds.xform(self.face_name + ".vtx[14]", q=True, ws=True, t=True)
        else:
            # posicion vertice superior del labio
            pos1 = cmds.xform(self.face_name + ".vtx[13]", q=True, ws=True, t=True)

            # posicion vertice inferior del labio 
            pos2 = cmds.xform(self.face_name + ".vtx[18]", q=True, ws=True, t=True)

        # creacion del plano del vertice superior
        mesh1 = cmds.polyCube( h=0.01, w = 8, d = 8 )[0]
        cmds.select(mesh1)
        cmds.move(pos1[0], pos1[1], pos1[2])
        cmds.rotate(0, 0, '45deg', r=True )

        # creacion del plano del vertce inferior
        mesh2 = cmds.polyCube( h=0.01, w = 8, d = 8 )[0]
        cmds.select(mesh2)
        cmds.move(pos2[0], pos2[1], pos2[2])
        cmds.rotate(0, 0, '-45deg', r=True )

        # Obtencion interseccion entre los planos del vertice 1 y del vertice 2
        tract_intersection = cmds.polyCBoolOp(mesh1, mesh2, op = 3, ch = 1, preserveColor = 0, classification = 1)[0]

        # Bake la interseccion de los planos
        cmds.select(tract_intersection)
        utils.bakeCustomToolPivot(pos = 1, ori = 0)

        # Obtencion de la posicion de la inteseccion y del borde del tracto
        intersection_pos = cmds.xform(tract_intersection, q=True, ws=True, t=True)
        tract_border_pos =cmds.xform(tract_border , q=True, ws=True, rp=True )

        # Se mueve la posicion del pivote del tracto bocal a la posicion 
        # De la curva del extremo del tracto vocal
        cmds.xform(tract_name, 
            piv = [tract_border_pos[0], tract_border_pos[1], tract_border_pos[2]]
            )

        # Se hace el bake del tracto vocal
        cmds.select(tract_name)
        utils.bakeCustomToolPivot(pos=1, ori=0)

        # se mueve el tracto vacal teneindo en cuenta la posicion
        #  de la intereseccion de los planos del vertice y la 
        # posicion de la curva del extremo de la boca
        cmds.select(tract_name)
        cmds.move( intersection_pos[0], lips_border_pos[1], lips_border_pos[2])

        # Se borran los objetos para hacer la interseccion
        cmds.delete(mesh1)
        cmds.delete(mesh2)
        cmds.delete(tract_intersection)
