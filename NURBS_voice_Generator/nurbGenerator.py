# -*- coding: utf-8 -*-

import maya.cmds as cmds
import time
import random
import re
import numpy as np
import math
from functools import partial
from OBB.api import OBB
import maya.mel as mel
import nurbSection

class NurbGenerator(object):

    
    nurbs = []
    orderedNurbs = []

    def __init__(self):
        """Constructor"""
        print("NurbGeneratosContructor")

    def DeleteAttachedNurbs(self, nurbs):
        """Se borran los nurbs que sobran una vez hecho el attach"""
        if nurbs == None:
            return 
        if isinstance(nurbs[0], nurbSection.NurbSection) == False: 
            return

        for y in range(0, len(nurbs) -1):
            cmds.delete(nurbs[y].getName())

    def MakeAttach(self, nurbs):
        """ Se hace el attach de los nurbs"""

        # Si nurbs es none o solo hay uno no hace el attach
        if not isinstance( nurbs, (frozenset, list, set, tuple) ) :
            return nurbs.getName()


        # Se setea el valor inicial para empezar a hacer los attach en bucle 
        retNurb = nurbs[0].getName()
        startNurb = retNurb
        final_ret = None
        nurbs_len = len(nurbs)
        # Se hace el attach de los nurbs
        for y in range(0, nurbs_len):
            if y == 0:
                continue
            retNurb = cmds.attachSurface(nurbs[y].getName(),retNurb, m = 1 , kmk=False)

            final_ret = retNurb[0]
            retNurb = retNurb[1]
        
        # Se borran los nurbs
        self.DeleteAttachedNurbs(nurbs)
        return final_ret
            
    def MakeSections(self, sectionSize):
        """Se hacen los grupos de secciones"""

        attached_elements = []

        # En el caso de que el tamaño de los gtupos de devuelcen los
        #  propios nurbs ordenadors sin hacer ninguna unión
        if sectionSize == 1:
            for x in range(0, len(self.orderedNurbs)):
                attached_elements.append(self.orderedNurbs[x].getName())
            return attached_elements
                
        # Se hacen las uniones de nurbs para gegerar los grupos de secciones
        total_nurbs = len(self.orderedNurbs)
        numberOfGroups = total_nurbs/sectionSize
        resto = total_nurbs % sectionSize

        numberOfGroups = math.ceil(numberOfGroups)
        fist_element_last_group_number = 0
        
        for x in range(0, int(numberOfGroups) ):

            attached_element = self.MakeAttach(self.orderedNurbs[x*sectionSize:(x+1)*sectionSize])
            
            attached_elements.append(attached_element)
            fist_element_last_group_number = (x+1)*sectionSize 
            
        # En el caso de que todos los nurbs ya se hayan agrupados querra
        # decir que ya se pueden decolver los grupos de secciones
        if fist_element_last_group_number >= (len(self.orderedNurbs)):
            print attached_elements
            return attached_elements

        # Si han quedado algunos nurbs sin agrupar se agrupan 
        # todos los nurbs restantes en una seccion
        if resto == 1:
            attached_element = self.MakeAttach(self.orderedNurbs[int(fist_element_last_group_number )])
            attached_elements.append(attached_element)

        else:
            attached_element = self.MakeAttach(self.orderedNurbs[int(fist_element_last_group_number) : ])
            attached_elements.append(attached_element)
        
        print attached_elements
        return attached_elements

    def isInExtreme(self, current_nurb, old_Nurb):
        """Se devuelve si el current Nurb esta situado en el extremo del tracto vocal"""

        # En el caso de old_Nurb 
        if old_Nurb == None:
            return

        # En el caso de que el old Nurb(el antiguo nurb vecino que ya 
        # se ha analizado) no tenga un segundo nurb mas cercano querra
        # decir que el nurb actual no esta en ele extremo
        if old_Nurb.getSeconNearestSinglePath() == None:
            return False

        # En el caso de que el nurb mas cercano no tenga un segundo nurb mas cercano y teniendo en cuenta que no se ha cumplido la condicion anterior
        if current_nurb.getSeconNearestSinglePath() == None:
            return True

        # Se hacen comprobaciones para saber si se esta en el extremo
        fstOldNearestNurb = old_Nurb.getFirstNearestSinglePath().getName()
        sndOldNearestNurb = old_Nurb.getSeconNearestSinglePath().getName()        
        

        fst_Old_Nurb = current_nurb.getFirstNearestSinglePath().getName()
        snd_Old_Nurb = current_nurb.getSeconNearestSinglePath().getName()

        if sndOldNearestNurb == fst_Old_Nurb or snd_Old_Nurb == fstOldNearestNurb or snd_Old_Nurb == sndOldNearestNurb or fst_Old_Nurb == fstOldNearestNurb:
            return True
        return False
        
    def findNurb(self, nurb_name):
        """Se busca si el nurb name es el nombre
         de algun nurb del array de self.nurbs
         """
        for nurb in self.nurbs:
            if nurb_name == nurb.getName():
                return nurb
        return None

    def mergeAllNurbs(self, attached_elements):
        """Se unene todos los grupos de secciones en un nurb y se cierra el corte lateral"""
        #En el caso que no existan nurbs a unir no se unen nurbs
        if attached_elements == None or len(attached_elements) == 0 :
            return
            
            
        # En el caso de querer invertir el orden de union de 
        # los grupos de secciones se invierte el orden de la lista
        if self.reverse == True:
            attached_elements.reverse()

        #Se unen todos los grupos de secciones
        auxNurb = attached_elements[0]
        startNurb = auxNurb
        final_ret = None
        for y in range(1, len(attached_elements) ):
            
            if attached_elements[y] == None:
                return auxNurb

            auxNurb = cmds.attachSurface(attached_elements[y],auxNurb, m = 0 , kmk=False)
            cmds.delete(auxNurb[1])
            
            auxNurb = auxNurb[0]
        
        #se cierra el corte lateral del tracto
        cmds.closeSurface(auxNurb, ps = 2, bb = True, d = 0, rpo = True)
        
        # Se devuelve el nurb del tracto
        return auxNurb
    

    def setReverseOrder(self, reverse):
        """Set invertir orden agrupacion nurbs"""
        self.reverse = reverse

    def setNames(self, mesh_name):
        """Aignacion nombres tracto_vocal"""
        self.subdiv_name = mesh_name + "_subdiv"
        self.nurb_name = mesh_name + "_nurbs"

    def rebuildOrderedNurbs(self):
        """ Se hace el rebuild de todas las secciones(nurbs)"""
        for nrb in self.orderedNurbs:
            # Se hace el rebuild de cada uno de los nurbs que han sido ordenados
            cmds.select(nrb.getName())
            name = cmds.rebuildSurface( ch = 1, su = 44, kr = 0, dir = 2, rt=0, du = 3, dv = 3, rpo =1, end  =1, kc = 0, kcp = 0, tol =0.01, fr = 0)

    def makeNurbs(self,  mesh_name):
        """Se convierte la malla del tracto vocal con las secciones ya extraidas a nurbs y se ordenan"""

        # Creacion del array de nursb
        self.nurbs = []

        # Se hace el set de los nombres del tracto
        self.setNames(mesh_name)
        cmds.select(mesh_name)
        
        # Se convierte la mesh a sibdiv y se cambia el nombre de este subdiv
        mesh = cmds.ls(sl = True)
        subd = cmds.polyToSubdiv(mesh, ch=False, preserveVertexOrdering=True, maxPolyCount = 2000, quickConvert = False, maxEdgesPerVert = 60)[0]
        subd = cmds.rename(subd, self.subdiv_name)

        # Se convierte el subdiv a Nurbs y se cambiael nombre
        nurbsConvert = cmds.subdToNurbs(subd, ch=False)[0]
        nurbsConvert = cmds.rename(nurbsConvert, self.nurb_name)
        
    

        cmds.delete(self.subdiv_name)
        cmds.delete(mesh_name)
        
        # Seleccion de las superficies de los nurbs
        cmds.select(self.nurb_name)
        nurb_objects = cmds.listRelatives(ad = True, type = "nurbsSurface")

        # Por cada seccion nurb 
        for nurb_object in nurb_objects:
            try:
                # Se centra el pivote de la seccion
                cmds.xform(nurb_object, cp = True, p = True)
                
                # Se crea una malla a prtir del nurb de la seccion para 
                # crear una bounding box sin tenener en cuenta los ejes
                cmds.select(nurb_object)
                mesh_nurb = cmds.nurbsToPoly(nurb_object)
                cmds.select(mesh_nurb)
                obbBoundBoxHull = OBB.from_points(mesh_nurb[0])

                obbCube = cmds.polyCube(
                    constructionHistory=False, name="hullMethod_GEO")[0]

                cmds.xform(obbCube, matrix=obbBoundBoxHull.matrix)
                
                # Se obtien la posicon de la seccion
                pivot_array = cmds.xform( q=1,ws=1,rp=1 )
                pivot_vec = np.array([pivot_array[0], pivot_array[1], pivot_array[2]])

                # Se guarda la informacion que corresponde a esta seccion(nurb) 
                print ("antes contructor")
                aux_nurb = nurbSection.NurbSection()
                aux_nurb.setNameAndPosition(nurb_object, pivot_vec)
                print ("Despues contructor")
                aux_nurb.setBoundingBox(obbCube)

                # Se añade el objeto con la informacion den nurb a un array 
                self.nurbs.append( aux_nurb )
                # Se elimina la malla con la forma del nurb debido a que 
                # unicamente se utiliza para obtener la bounding box que
                # no tenga en cuenta los ejes
                cmds.delete(mesh_nurb)
            except:
                print("not valid object")

        # Se buscan los dos nurbs mas cercanos, o en el caso
        #  de que este en el extremos, el mas cercano
        for nurb in self.nurbs:
            nurb.searchNearestsPivots(self.nurbs)

        # Array que contiene los nurbs ordenados
        self.orderedNurbs = []

        i = 0
        endDirection = False
        currentPivot =  None
        currentPivotName = None
        previuos_pivot = None
        previuos_pivot_name = None
        print(self.nurbs)

        # Se hace un bucle para forzar que todos los nurbs que hay 
        # dentro del array self.nurbs esten en el array de nurbs ordenados
        while i < len(self.nurbs):
            # En el caso de que sea la primera vuelta del bucele se asigna añade un nur_ooridenado inicial al array
            if i == 0:
                #Current pivot name es el nurb que va a analizar en la siguiente iteracion
                currentPivotName = self.nurbs[0].getFirstNearestSinglePath().getName()
                # En la siguiente iteriacion el nurb anterior al previuous pibot name sera 
                # el que se añada al array de nurbs ordenados
                previous_pivot_name = self.nurbs[0].getName()
                self.orderedNurbs.append(self.nurbs[0])

            # En el caso de que no sea la primera iteracion:
            else:
                # Se buca el nurb que se va a innsertar en esta iteracion, y los dos nurbs mas cercanos a este y 
                currentPivot = self.findNurb(currentPivotName)
                previuos_pivot = self.findNurb(previous_pivot_name)

                first = currentPivot.getFirstNearestSinglePath()
                second = currentPivot.getSeconNearestSinglePath()
                
                # En el caso de que el nurb mas cercano del nurb que se va inserar 
                #  no este en el array de nurbs ordenados, el nurb actual pasara a 
                # ser el el primer nurb mas cercano
                if first.isInArray(self.orderedNurbs) == False:

                    # En el caso de no haber encontrado un extremo se añade el nurb actual al final del array de los nurbs ordenados
                    if endDirection == False:
                        self.orderedNurbs.append(currentPivot)

                        # En la siguiente iteriacion el nurb anterior sera 
                        # el que se añada al array de nurbs ordenados
                        previous_pivot_name = currentPivot.getName()

                        currentPivotName = first.getName()

                    # En el caso de haber encontrado ya el extremo el nurb actual se añade en la poscion 0 del array de los nurbs ordenados
                    else:
                        self.orderedNurbs.insert(0, currentPivot)

                        # En la siguiente iteriacion el nurb anterior sera 
                        # el que se añada al array de nurbs ordenados
                        previous_pivot_name = self.orderedNurbs[0].getName()

                        currentPivotName = first.getName()

                # Si el primer nurb mas cercano no esta en el array de nurbs ordenados
                else:
                    # En el caso de que se este en un nurb del extremo, se añade el nurb actual al 
                    if  self.isInExtreme(currentPivot,previuos_pivot) == True and endDirection == False:
                        self.orderedNurbs.append(currentPivot)

                        # En la siguiente iteriacion el nurb anterior sera 
                        # el que se añada al array de nurbs ordenados
                        previous_pivot_name = self.orderedNurbs[0].getName()

                        if self.orderedNurbs[0].getSeconNearestSinglePath() != None:
                            currentPivotName = self.orderedNurbs[0].getSeconNearestSinglePath().getName()
        
                        endDirection = True

                    else:
                        # En el caso de haber encontrado ya el extremo el nurb actual se añade en la poscion 0 del array de los nurbs ordenados
                        if endDirection == True:
                            self.orderedNurbs.insert(0, currentPivot)

                            # En la siguiente iteriacion el nurb anterior sera 
                            # el que se añada al array de nurbs ordenados
                            previous_pivot_name = self.orderedNurbs[0].getName()

                            if self.orderedNurbs[0].getSeconNearestSinglePath() != None:
                                currentPivotName = self.orderedNurbs[0].getSeconNearestSinglePath().getName()
                        
                        # En el caso de no haber encontrado un extremo se añade el nurb actual al final del array de los nurbs ordenados
                        else:
                            self.orderedNurbs.append(currentPivot)

                            # En la siguiente iteriacion el nurb anterior sera 
                            # el que se añada al array de nurbs ordenados
                            previous_pivot_name = currentPivot.getName()

                            currentPivotName = second.getName()

            i = i + 1
        print(self.orderedNurbs)

        for a_nurb in self.orderedNurbs:
            a_nurb.deleteBoundingBox()
        
        
    def attachNurbs(self, group_size, option):
        """Se hacen grupos de secciones y secrea un nurb final"""
        
        # Se generan los grupos de secciones
        self.attached_nurbs = self.MakeSections(group_size)

        # Se hace la union de los frupos de secciones
        self.final_nurb = self.mergeAllNurbs(self.attached_nurbs)


    def selectTractAndUnion(self):
        """Se hace la seleccion de nurb que contiene el tracto y la uniñon"""
        cmds.select(self.final_nurb, add = True)