# -*- coding: utf-8 -*-


import maya.cmds as cmds
from functools import partial

import morpherAdapter
import meshAdapter
import nurbGenerator
import utils 
import fileExporter

class Ventana(object):


    def AdaptMesh(self, *args):
        """ Se unen todos los objetos del tracto en uno"""
        self.final_mesh_name = cmds.textField(self.name,  q=True, text=True)

        # se unen todos los objetos que forman parte del tracto vocal y se unen en uno.
        # Tmbien se unen los vetrivces que estan en la misma posición
        self.meshAdapter.AdaptMesh(self.final_mesh_name)
        
        # Se borran tos triangulos del extremo del tracto vocal y se obtene el borde de edges que conecta con la boca
        self.meshAdapter.removeAllTriangles()

    def makeSections(self, *args):
        """ Se seccionan la malla en secciones """
        # Seccionamiento del tracto vocal

        self.meshAdapter.makeSections()

    def GenerateNurbs(self, *args):
        """ Se converte la malla seccioda a secciones en nurbs """
        #Se convierte el tracto a nurbs y se se guarda en un array el orden correcto de las secciones en nurbs
        self.nurbsGenerator.makeNurbs(self.meshAdapter.getName())

    def mergeNurbs(self,*args):
        """ Se hace el merge de los nurbs de las secciones """
        reverse =  cmds.checkBox (self.reverse, q = True, value = True)
        group_size =  cmds.intSliderGrp(self.seccionsOfGroup ,q=True,v=True)
        self.nurbsGenerator.setReverseOrder(reverse)

        # Se especifican las caractericrcas del rebuild y se hace la union de los nurbs
        self.nurbsGenerator.attachNurbs(group_size, 1)


    def importBocalTract(self,*args):
        """ Importacion del tracto vocal"""
        # Indicacion de los posibles formatos de los archicvos de tractos vocal
        basicFilter = "*.igs *.iges *.dxf"

        #obtención del path del archivo
        fPath =cmds.fileDialog2(fileFilter=basicFilter, dialogStyle=2, fileMode = 1)
        
        # Importacion del tracto vocal
        tract_objs = self.fm.importTract(path = fPath)
        self.meshAdapter.setTractObjs(tract_objs)

    def exportNurbs(self, *args):
        """ Exportacion de los objetos del tacto con la union y la boca"""
        # Se abre un exporador de archivos para seleccionar el directiorio done guardar el archivo
        fPath =cmds.fileDialog2( dialogStyle=2, fileMode = 3)

        # En el caso de que el usuario haya cancelado el paso anterior se cancela el proceso de exportar
        if fPath == None:
            return

        # Se seleccionan unicamente los nubs que contienen el tracto vocal con la union y la boca
        cmds.select(cl = True)
        self.nurbsGenerator.selectTractAndUnion()
        file_name= cmds.textField(self.name,  q=True, text=True)

        fPath = fPath[0] + "/" + file_name

        cmds.file(fPath,  es = True, type = "IGESExport")

    def __init__(self):
        """ Crea la ventana"""
        # Se crean las clase
        self.nurbsGenerator = nurbGenerator.NurbGenerator()
        self.fm = fileExporter.FileManager()
        self.meshAdapter = meshAdapter.MeshAdapter()

        nombre_de_ventana = 'conevrsionTracto'
        
        # se crea la ventana. en el caso de que ya exista, se cierra la antigua
        if cmds.window(nombre_de_ventana, exists = True):
            cmds.deleteUI(nombre_de_ventana)
        ventana = cmds.window(nombre_de_ventana, resizeToFitChildren=True, width = 400)

        #Creacion de las dimensiones de la ventana y el estilo
        cmds.columnLayout(columnAttach=('both', 10), rs = 7, columnWidth=400)

        #creación de los botones para importar
        cmds.button('Importar tracto vocal', command = self.importBocalTract)        
        
        # Asignacion del nombre del nurb del tracto bocal
        # Este mismo nombre sera el del arxivo cuando se exporte
        cmds.text("Introduzca el nombre del tracto antes de unir todos los objetoss que forman el tracto.", wordWrap = True)
        cmds.rowLayout(numberOfColumns=2, adjustableColumn=1,columnWidth2=[150, 250])
        name = cmds.text("Nombre: ")
        self.name = cmds.textField(ed = True, width = 250)
        cmds.setParent('..')

        # Boton pa hacer el merge de todos los objetos que forman el tracto vocal
        cmds.button('Unir todos los objetos del tracto en uno', command = partial(self.AdaptMesh))

        # Se secciona el tracto vocal y se eliminan las secciones del extremo del tracto que se hayan indicado anteriormente.
        cmds.button('Seccionar tracto vocal', command = partial(self.makeSections))
        
        # Indica el Metodo a utilizar para unir los nurbs
        cmds.text("Introduce los parametros para generar el tracto vocal en nurbs.", wordWrap = True)
        
        # Seleccion del numero de nurbs que unir en uno solo.
        cmds.rowLayout(numberOfColumns=2,columnWidth2=[100, 300])
        cmds.text("Agrupar secciones en un grupos de:", wordWrap = True, width = 100)
        self.seccionsOfGroup = cmds.intSliderGrp(v = 2, min = 1, max = 6, width = 280, s=1, field = True )
        cmds.setParent('..')

        # Boton para convertir los poligonos del tracto a Nurbs.
        # Una vez convertido se crea un array para saber el orden de los nurbs
        cmds.button('Generar Nurb/s', command = partial(self.GenerateNurbs))
        # Checkbox para invertir el orden de la union
        self.reverse = cmds.checkBox (l = "Reverse direction", value = False)
        # Boton para unir todos los nurbs
        cmds.button('Unir Nurbs', command = partial(self.mergeNurbs))

        # Boron para exportar el nurb del tracto con la union y la boca
        cmds.button('Export', command = partial(self.exportNurbs))

        # Se muestra la ventana
        cmds.showWindow(nombre_de_ventana)