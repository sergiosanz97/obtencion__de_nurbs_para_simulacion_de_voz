# -*- coding: utf-8 -*-

import maya.cmds as cmds
import time
import random
import re
import numpy as np
import math
import maya.mel as mel
import vertexToCurveDistance
import utils

class MeshAdapter(object):
    

    final_mesh_name = ''    
    n_tract_loop = 48
    s = "abc123AUG|GAC|UGAasdfg789"
    vtx_pattern = "tracto_vocal.vtx[(.*?)]"
    face_pattern = "tracto_vocal.f[(.*?)]"
    edge_pattern = "tracto_vocal.e[(.*?)]"
    extrudedPolys = []
    break_edges = []
    limit_vertices = []

    def __init__(self):
        """Constructor"""
        # Se crean arrays
        self.extrudedPolys = []
        self.break_edges = []
        self.limit_vertices = []

    def setPatterns(self):
        """set Patterns"""
        self.vtx_pattern = self.final_mesh_name + '.vtx[(.*?)]'
        self.face_pattern = self.final_mesh_name + '.f[(.*?)]'
        self.edge_pattern = self.final_mesh_name + '.e[(.*?)]'


    def getFirstEdge(self, borders):
        """Se obtiene el primer elemento de una lista de edges"""
        for edge in borders :
            range =  edge[edge.find("[")+1 : edge.find("]")]
            print range
            if ":" in range:

                numbers_of_range = self.GetNumbersBetweenRange(range)
                return int(numbers_of_range[0])
            else:
                return range

    def getFaceCurve(self):

        """Se obtiene la curva y el borde del extremo de la cara que toca con el tracto"""
        # Array donde an cada indice esta la curva de un extremo
        curves = []

        # Array donde en cada indice estan losd los edges de un extremo
        border = []

        # Seleccion de todos los edges que no pertences a ningun extremo.
        cmds.select(self.face_name + ".f[*]")
        mel.eval('PolySelectTraverse 2')
        cmds.select( cmds.polyListComponentConversion( tv=True ,ff = True) )   
        cmds.select( cmds.polyListComponentConversion( te=True ,fv = True) )   
        not_border_edges = cmds.ls(sl = True)

        # Seleccion y obtención de lso edges del borde
        cmds.select(self.face_name +'.e[*]')
        cmds.select(not_border_edges, d = True)
        all_borders_edges = cmds.ls(sl = True)

        # Creo la primera curva para uno de los bordes
        print("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
        # Para obtener los edges del primer borde se coge el un poligono de los edges que 
        # son bordes y se secciona todo el borde a pertir de este
        cmds.polySelect(self.face_name, eb = int(self.getFirstEdge(all_borders_edges)))
        border1 = cmds.ls(sl = True)

        nurve  =cmds.polyToCurve( nds = 5)
        curves.append(nurve[0])
        border.append(border1)
        print("llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll")
        #creo la otra curva para el otro borde
        # Se seleccionan todos los edges y se deseleccion los edges que no estan en 
        # los extremos y los bordes del extremo que que se ha obtenido anteriomente
        cmds.select(self.face_name + '.e[*]')
        cmds.select(not_border_edges, d = True)
        cmds.select(border1,d = True)
        border2 = cmds.ls(sl = True)
        nurve = cmds.polyToCurve(form = 2, degree = 3 ,conformToSmoothMeshPreview = 1)
        curves.append(nurve[0])
        border.append(border2)
        
        #se calcula el edge mas grande de cada una de las bounding box de las curvas de los extremos.
        maxedge0 = self.getMaxEdge(curves[0])
        maxedge1 = self.getMaxEdge(curves[1])

        # La bounding box que tenga el menor boun

        if maxedge0 < maxedge1:
            self.face_border = curves[0]
            self.border_face_edge = border[0]
            cmds.delete(curves[1])
        else:
            self.face_border = curves[1]
            self.border_face_edge = border[1]
            cmds.delete(curves[0])

    def getBorderFaceEdge(self):
        """ Devuelve los edges del borde de la boca que conecta con el tracto"""
        return self.border_face_edge

    def clear(self):
        """Limpia informacion de la clase"""
        self.extrudedPolys = []
        self.break_edges = []
        self.limit_vertices = []
        self.final_mesh_name = ''

    def setTractObjs(self, tract_objs):
        """se asigna el nombre del tracto"""
        self.tract_objs  = tract_objs

    def AdaptMesh(self,mesh_name):
        """ Todos los objetos que forman parte del tracto se unen en uno y se unen sus vertices"""
        #Seleccion de todos los objetos que forman el tracto vocal
        cmds.select(self.tract_objs)
        
        a = cmds.ls(sl =True)
        self.final_mesh_name = mesh_name

        if len(a) > 1:
            #se unen todos los objetos del tracto en uno                                                                                                            
            aux_mesh = cmds.polyUnite( centerPivot = True, 
                n= self.final_mesh_name, ch = True, mergeUVSets = True
                )

            # Union de los vertices que esten en la misma posición
            cmds.polyMergeVertex(aux_mesh)
            cmds.delete(a)
            if(aux_mesh != self.final_mesh_name):
                cmds.rename(aux_mesh[0], self.final_mesh_name)

        # En el caso de que el nunero de objetos que forman el 
        # tracto sea uno se hace unicamente el rename
        elif len(a) == 1:
            self.final_mesh_name = cmds.rename(a[0], self.final_mesh_name)    

    def getName(self):
        """Se obtiene el nombre del tracto"""
        return self.final_mesh_name
    
    def GetStringBetween(self, string, pattern):
        """"Se obtienen los indices de eleemntos"""
        #Obtencion de los indices de un vertex, face o edges
        
        substring = string[string.find("[")+1 : string.find("]")]
        return substring
    
    def GetNumbersBetweenRange(self, string):
        """se obtienen los numeros entre los intervalos de nos numeros separados por :"""
        # Obtencion en un array de los indices entre "[", ":" y "]
        numbers = []
        margin_numbers = string.split(":")

        # Obtencion del rango de de numeros que hay entre los dos numeros 
        distance = int(margin_numbers[1]) - int(margin_numbers[0]) 

        #Asignacion de los numers
        for i in range (0,distance +1):
            numbers.append(int (margin_numbers[0]) + i)
            
        return numbers
        
    
    def PolyIsTriangle(self, poly_edges):
        """Se devuelve si es un triangulo"""
        is_triangle = False

        # Si el numero de edges de una face es igual  a 3 es un triangulo
        if len(poly_edges) == 3:
            is_triangle = True
        else:
            is_triangle = False
     
        return is_triangle
    
    def CalculateNumberOfEdges(self, edges):
        """Se calcula el numero de edges que se pasan"""
        #Numero de edges totales 
        total_edges = 0

        
        # Se miran todos los elemtos del array de edges
        for edge in edges:
            # en el caso de que la posicion del array contenga un intervalo de edges.
            # Se añade la cantidad de numeros que hay entre ese intervalo
            if ":" in edge:
                number_range = self.GetStringBetween(edge,self.edge_pattern)
                numbers = self.GetNumbersBetweenRange(number_range)
                total_edges = total_edges + len(numbers)
            # En el caso de ser un unico edge se añade uno 
            else:
                total_edges = total_edges + 1
                
        return total_edges
    
    
    def GetCentralVerticeOfExtreme(self, poly_edges):
        """Se devuelve el vertice central del extremo"""
        
        edges_from_face = []        
        
        poly_edges_len = len(poly_edges)

        # Se añaden los edges a analizar.
        for i in poly_edges:
            edges_from_face.append(self.final_mesh_name  + ".e[" + i + "]")
        
        # Se convierten los edges de vertices.
        cmds.select(edges_from_face)
        cmds.select( cmds.polyListComponentConversion( tv=True ,fe = True)) 
        
        #Lista de todos los vertices del triangulo
        vertices_from_edges = cmds.ls(sl = True)

        # se mira por cada vertice si es el vertice central
        for vertice_from_edge in vertices_from_edges :

            # en el caso de la posicion contenga un 
            # intervalo mirmamos los numero del intervalo
            if ":" in vertice_from_edge:
                #Extraccion numeros del intervalo
                range = self.GetStringBetween(string = vertice_from_edge, pattern = self.vtx_pattern)
                numbers_of_range = self.GetNumbersBetweenRange(range)

                # Se analiza numero a numero del intervalo
                for current_vertex in numbers_of_range:
                    # Se mira si al convertir el vertice a edges,
                    # si el numero de edges es mayor a 4,
                    # para saber si es el vertice del extremo 
                    return_vertex = self.final_mesh_name  + ".vtx[" + str(current_vertex) + "]"
                    cmds.select(return_vertex)
                    cmds.select( cmds.polyListComponentConversion( te=True ,fv = True))
                    edge_neighbours = cmds.ls(sl = True)
                    length_of_edge_range = self.CalculateNumberOfEdges(edge_neighbours)
                    
                    if length_of_edge_range > 4:
                        return return_vertex
            
            #En el caso de que unicamente haya seleccionado un unico vertex
            # Se verifica si este vertex es el del extremo
            else:
                # Se mira si al convertir el vertice a edges,
                # si el numero de edges es mayor a 4,
                # para saber si es el vertice del extremo 
                cmds.select(vertice_from_edge)
                cmds.select( cmds.polyListComponentConversion( te=True ,fv = True))
                edge_neighbours = cmds.ls(sl = True)
                length_of_edge_range = self.CalculateNumberOfEdges(edge_neighbours)  
    
                if length_of_edge_range > 4:
                    return vertice_from_edge

        return None
    
    def GetFacesFromLimit(self, vertex):
        """Se devuelven los faces que rodean un vertice"""
        # Se convierte el vertice del extremo a faces para obtener los poligonos del extremo
        cmds.select(vertex)
        cmds.select( cmds.polyListComponentConversion( tf=True ,fv = True))
        face_neighbours = cmds.ls(sl = True)
                            
        return face_neighbours
    
    def addPolystoExtrudedPolys(self, polys):
        """Se añaden poligonos al array que contiene los poligonos"""     
        self.extrudedPolys.extend(polys)
    
    def IsThisPolyExtruded(self, poly):
        # Se mira si el edge que se introduce como parametro  ya ha sido extruido.
        # Se mira una a una las faces que han sio extruidas, 
        # y en el caso de que sea igual al poligono introducido
        # querra decir que ya se ha extruido
        for extrude_poly in self.extrudedPolys:
            if ":" in extrude_poly:
                number_range = self.GetStringBetween(extrude_poly,self.face_pattern)
                numbers = self.GetNumbersBetweenRange(number_range)
                
                for number in numbers:
                    poly_str = self.final_mesh_name + ".f[" + str(number) + "]"
                    
                    if poly_str == poly:
                        return True
                    
            if poly == extrude_poly:
                return True
                
        return False
    
    def ExtrudeLimitTriangles(self, faces):
        """Extraccion de una seccion"""
        # Se extraen las faces de la malla
        cmds.select(faces)
        cmds.ExtractFace(faces)
    
    def getMaxEdge(self, objectName):
        """Se obtiene el lado mas grande de la bounding box de objectName"""
        edge1 = cmds.exactWorldBoundingBox(objectName)
        edges = []

        # Se obtiene el tamaño de cada lado de la bounding box
        edges.append(abs(edge1[0] - edge1[3]))
        edges.append(abs(edge1[1] - edge1[4]))
        edges.append(abs(edge1[2] - edge1[5]))

        # Se devuelve el mayor lado de la bounding box
        return max(edges)

    def getBocalEdge(self):
        """ Se devuelve """
        return self.bocalEdge 

    def setBocalEdge(self):
        """Se asignan los edges que contactan con la boca"""

        #Se cenytran los poivotes de las curvas de los extremos
        cmds.select(self.edges_extremes[0])
        cmds.xform( cp =True)
        utils.bakeCustomToolPivot(pos = 1, ori = 0)

        cmds.select(self.edges_extremes[1])
        cmds.xform( cp =True)
        utils.bakeCustomToolPivot(pos = 1, ori = 0)

        #Se obtienen la posiciones de la y de las  curvas de los extemos
        curve0_y_pos = cmds.xform(self.edges_extremes[0],q=1,ws=1,rp=1)[1]
        curve1_y_pos = cmds.xform(self.edges_extremes[1],q=1,ws=1,rp=1)[1]

        # La curva que este mas alta, sera la que contacta con la boca
        if curve1_y_pos < curve0_y_pos:
            cmds.delete(self.edges_extremes[1])
            self.bocalEdge = self.edges_extremes[0]
        else:
            cmds.delete(self.edges_extremes[0])
            self.bocalEdge = self.edges_extremes[1]
        
    def unlinkCurve(self, name):
        """Se desvincula la curva que se pasa como parametro"""
        # Se desvincula la curva de la malla a partir de la que ha sido extraido
        destinationAttrs = cmds.listConnections(str(name + ".outputcurve"), plugs=True, source=False) or []
        sourceAttrs = cmds.listConnections(str(name + ".outputcurve"), plugs=True, destination=False) or []
        for destAttr in destinationAttrs:
            cmds.disconnectAttr(str(name + ".outputcurve"), destAttr)
        for srcAttr in sourceAttrs:
            cmds.disconnectAttr(srcAttr, str(name + ".outputcurve"))

    def removeAllTriangles(self):
        """Se borran todos los triangulos y se obtiene la curcva del extremo que contacat con la boca"""
        self.edges_extremes = [] 
        
        # Hsasta que no se hayan mirado todos los poligonos se analiza si el poligono es un traingulo
        while True:
            cmds.select(self.final_mesh_name)

            # Se mira poligono a poligono si es un triangulo
            for i in range(0, cmds.polyEvaluate(f = True)):

                face = self.final_mesh_name + ".f[" + str(i) +  "]"
                cmds.select(cl = True)
                cmds.select(face)
                edgesInfo = cmds.polyInfo(faceToEdge = True)
                edges = edgesInfo[0].split()
                del edges[0:2]

                if self.PolyIsTriangle(edges):
                    limit_vertex = self.GetCentralVerticeOfExtreme( edges)

                    # Seleccion de los poligonos del borde
                    limit_faces = self.GetFacesFromLimit(limit_vertex)

                    cmds.select(limit_vertex)

                    # Se guardan los edges que pertenecen al extremo del tracto pero que no pertenecen al borde
                    vertex_limit_edges = cmds.polyListComponentConversion(fv = True, te = True)

                    # Se seleccionan todos los edges que pertenecel al extremo
                    cmds.select(cmds.polyListComponentConversion(fv = True, tf = True))
                    cmds.select(cmds.polyListComponentConversion(ff = True, te = True))

                    # Se deselecciona los eges del extremo que o pertenecen al borde
                    cmds.select(vertex_limit_edges, d = True)

                    # Se cinvierten los edges del borde a una curva
                    nodes  =cmds.polyToCurve( nds = 5)
                    cmds.xform(cp =True)

                    # Se elimina el la union de nodos que vincula la curva con la malla.
                    # Para que al modificar la malla no se modifique la curva
                    self.unlinkCurve(nodes[1])

                    # Se añade la curva del extremo a un array que guarda los dos extremos del tracto
                    self.edges_extremes.append(nodes[0])
                    cmds.delete(limit_faces)
                    i = i-1

                cmds.select(self.final_mesh_name)
                if i == (cmds.polyEvaluate(f = True) -1):
                    self.setBocalEdge()
                    border_position = cmds.xform(self.bocalEdge,q=1,ws=1,rp=1)

                    cmds.move(border_position[0], border_position[1], border_position[2], self.final_mesh_name + ".scalePivot", self.final_mesh_name + ".rotatePivot", absolute=True)
                    return 

    def getBorderCurveEdge(self):
        """Se devuleven los edges del extremo que contactan con la boca"""
        return self.bocalEdge
    
    def ExtractSection(self, face):
        """Se extrae una seccion a la que pertence face en el caso de que no haya sido extraido anteriormente """
        # Si el poligono ya ha sido extruido se pasa al siguiente poligono
        if self.IsThisPolyExtruded(face) == True: 
            return
        
        cmds.select(face)
        edgesInfo = cmds.polyInfo(faceToEdge = True)
        edges = edgesInfo[0].split()
        
        del edges[0:2]
        
        # Se mira edge a edge si es posible hacer un anillo a partir del edge
        for a in range(0,len(edges)):
            try:
                # Se intenta hacer el edge ringa a pertir del edge
                # y se convierten a faces para guardarlso
                loop_edges = cmds.polySelect( self.final_mesh_name , edgeRing=int(edges[a]) )                    
                cmds.select( cmds.polyListComponentConversion( tf=True ,fe = True) )   
                
                a = cmds.ls(sl =True)
    
                polys_num = len(loop_edges) -1
                
                cmds.ls(sl = True)

                # en el caso de que el primer elemento del array del ring de edges
                #  y el el utlimeo, sean iguales querra decir que esta cerrado 
                if len(loop_edges) > 1 and loop_edges[len(loop_edges) - 1] == loop_edges[0]:
                    cmds.select(a)
                    cmds.ExtractFace(a)
                    a = cmds.ls(sl = True)
                    self.addPolystoExtrudedPolys(a)

                    return
            except:
                print("No loop")
    
    def detachEdges(self):
        """Se hace el detach de los edges del lado del tracto"""
        # se hace el detach de los edges del lado del tracto vocal
        cmds.select(self.break_edges)
        cmds.select( cmds.polyListComponentConversion( tv=True ,fe = True) )   
        break_vertices = cmds.ls(sl = True)
        cmds.polySplitVertex(break_vertices)

    def setCutEdges(self):
        """Se obtienen los los edges para hacer el corte lateral y se obtien los edges del borde que tiene contacto con la boca"""
        # Clase para obtener el vertex mas cercano a el cv[0] del la curva del borde de la boca
        vtcd = vertexToCurveDistance.VertexToCurveDistance(self.face_border  , self.final_mesh_name, self.bocal_border_curve )

        # Se seleccionan los vertices de los bordes y se guardan
        cmds.select(self.final_mesh_name + '.vtx[*]')
        mel.eval('PolySelectTraverse 3')
        border_vertices = cmds.ls(sl = True)

        ## Se busca el vertice mas cercano al cv[0] y se guarda eb vtcd
        for vertice in border_vertices :
            range =  vertice[vertice.find("[")+1 : vertice.find("]")]
            if ":" in vertice:

                numbers_of_range = self.GetNumbersBetweenRange(range)
                for number in numbers_of_range:
                    vtcd.addVertex(str(number))
            else:
                vtcd.addVertex( str(range))

        # Se convierte el vertex mas cercano a edges
        cmds.select(self.final_mesh_name + '.vtx[' + str(vtcd.getVertexNumber()) + ']')
        cmds.select(cmds.polyListComponentConversion(te = True, fv = True))
        edges = cmds.ls(sl = True)

        # Se mira cada edge, apara saber si ese edge es de borde o de corte
        for edge in edges:
            range =  edge[edge.find("[")+1 : edge.find("]")]
            
            # En el caso que la posicion del array contenga un rango de edges se mira cada uno de estos edges
            if ":" in edge:

                numbers_of_range = self.GetNumbersBetweenRange(range)

                for number in numbers_of_range:
                    #Se intenta obtener el borde a partir del edge y 
                    # se cuenta el numero de edges que se han seleccionado
                    cmds.polySelect( self.final_mesh_name , eb=int(number) )
                    
                    #La funcion Polyevaluate deveulve None en el 
                    # caso de que no haya ningun edge seleccionado 
                    number_of_selected_edges = cmds.polyEvaluate(ec = True)

                    cmds.select(cmds.polyListComponentConversion(tv = True, fe = True))
                    number_of_selected_vertices = cmds.polyEvaluate(vc = True)

                    # En el caso que el numero de edges sea un numero 
                    # querra decir que este edge pertenece al borde.
                    
                    if  utils.is_number_tryexcept(number_of_selected_edges) == False:
                        cmds.polySelect( self.final_mesh_name , edgeLoop=int(number) )
                        self.break_edges = cmds.ls(sl = True)
                        self.face_border_Edge =  number 
                    else :
                        # Se asigna un edge del bode para luego hacer la seleccion 
                        cmds.polySelect( self.final_mesh_name , eb=int(number) )
                        self.border_edge_tract = cmds.ls(sl = True)

            # Si la posicion del array contiene un unico edge
            else:

                cmds.polySelect( self.final_mesh_name , eb=int(range) )
                number_of_selected_edges = cmds.polyEvaluate(ec = True)

                cmds.select(cmds.polyListComponentConversion(tv = True, fe = True))
                number_of_selected_vertices = cmds.polyEvaluate(vc = True)

                # En el caso que el numero de edges sea un numero 
                # querra decir que este edge pertenece al borde.

                if  utils.is_number_tryexcept(number_of_selected_edges) == False:

                    cmds.polySelect( self.final_mesh_name , edgeLoop=int(range) )
                    self.break_edges = cmds.ls(sl = True)
                    self.face_border_Edge = range
                else:
                    # Se asigna un edge del bode para luego hacer la seleccion
                    cmds.polySelect( self.final_mesh_name , eb=int(range) ) 
                    self.border_edge_tract = cmds.ls(sl = True)

    def getLoftedName(self):
        """Return del la union entre la boca/cara y el tracto"""
        return self.lofted_name

    def getFaceBorder(self):
        """Retirn el nombre del borde de la cara/boca"""
        return self.face_border

    def getBorderCurve(self):
        """Devuelve el nombre de la curva del extremo"""
        return self.border_curve

    def setFaceName(self, name):
        """Se asigna el nombre de la cara"""
        self.face_name = name

    def removeSections(self, border_edge, section_number = 1):
        """Se obtienen los poligonos a borrar y los edges que conforman el nuevo edge teniendo en cuenta estos poligonos"""
        # En el caso de no haya ninguna seccion a eliminar no se devuelven faces a borra y se devuelve el mismo borde que se ha pasado en esta funcion
        if section_number == 0:
            return None, border_edge


        cmds.select(border_edge)
        border= cmds.ls(sl = True)

        # Se seleccionan y guardan los edges que se deseleccionaran 
        if section_number > 1:
            cmds.select(cmds.polyListComponentConversion( tf=True ,fe = True))

            for i in range(0, section_number -2):
                # Se amplia la seleccion
                mel.eval('PolySelectTraverse 1')
            cmds.select(cmds.polyListComponentConversion( te=True ,ff = True))

        cmds.select(cmds.polyListComponentConversion( tv=True ,fe = True))
        cmds.select(cmds.polyListComponentConversion( te=True ,fv = True))
        cl_edges = cmds.ls(sl = True)

        # Se seleccionan todos los poligonos a borrar 
        
        cmds.select(border_edge)
        cmds.select(cmds.polyListComponentConversion( tf=True ,fe = True))

        if section_number > 1:
            for i in range(0, section_number -1):
                # Se amplia la seleccion
                mel.eval('PolySelectTraverse 1')

        # Se gaurdan los poligonos a borrar
        polygons_to_remove =cmds.ls(sl = True)
        cmds.select(cmds.polyListComponentConversion( te=True ,ff = True))
        # Se deseleccionan los edges para deseleccionar
        cmds.select(cl_edges, d = True)
        extreme_border = cmds.ls(sl = True)

        return polygons_to_remove, extreme_border
    
    def getMouthExtremeBorder(self):
        """ return Edges del borde de la boca """
        return self.mouth_extreme_border

    def getMouthPolygonsToRemove(self):
        """ return los poligonos del borde de la boca"""
        return self.mouth_polygons_to_remove

    def setMouthExtremeBorder(self, mb):
        """set de lso edges del borde de la boca """
        self.mouth_extreme_border = mb

    def setMouthPolygonsToRemove(self, pr):
        """set de los poligonos del borde de la boca"""
        self.mouth_polygons_to_remove = pr

    def setNumberSectionToRemoveMouth(self, num):
        """Set de las secciones a borrar de la boca"""
        self.number_sections_to_remove_mouth = num

    def getNumberSectionsToRemoveMouth(self):
        """ Return del numero de secciones a borrar de la boca"""
        return self.number_sections_to_remove_mouth

    def setBocalBorderCurve(self, bbc):
        """Se asigna el nombre de la curva del borde de la baca"""
        self.bocal_border_curve = bbc 

    def getBocalBorderCurve(self):
        """Se devuleve el nombre de la curva del borde de la baca"""
        return self.bocal_border_curve

    def makeSections(self):
        """ Se hacen las secciones de la malla, incluido el corte lateral y la eliminacion de los poligonos del extremo"""

        self.extrudedPolys = []

        # seleccion de la maya del tracto
        cmds.select(self.final_mesh_name )
        self.setPatterns()

        # Obtener edges que cortan secciones
        self.setCutEdges()

        # Obencio edges del borde que conectaran con la boca
        # y de los poligonos que tendran que ser eliminados
        self.tract_polygons_to_remove, self.tract_extreme_border =self.removeSections(border_edge = self.border_edge_tract, section_number= self.getNumberSectionsToRemoveMouth())

        # Obtencion total de faces de la malla de la cara
        num_of_total_object_faces = cmds.polyEvaluate(self.final_mesh_name, face=1)

        # Se intenta hacer el extrude por cada face de la malla, en el caso de que ya se 
        # haya hecho el extrude de la seccion a la que pertenece no se hace la extraccion
        for i in range(0, num_of_total_object_faces -1):
            # Extraccion de la seccion del poligono
            self.ExtractSection(self.final_mesh_name + '.f[' + str(i) + ']')

        # Se separan en dos los edegs paracortar las secciones
        self.detachEdges()
        
        #Seleccion de los edges que conectan con la boca para convertirlo a una curva
        print("poly_to_select    " + str(self.tract_extreme_border))
        cmds.select(self.tract_extreme_border)
        nodes  = cmds.polyToCurve( nds = 5)

        # Se desvincula la curva creada de la malla para que al modificar la mall no se modifique la curva
        self.unlinkCurve(nodes[1])
        self.border_curve = nodes[0]

        # Rebuild de la curva
        cmds.select(self.face_border)
        cmds.rebuildCurve (ch = 1, rpo = 1, rt = 0, end = 0, kr = 2, kcp = 0, kep = 1, kt = 0, s = 44, d = 3, tol = 0.01)

        # eliminacion de las seccionas del exrtremo que contacta con el tracto voal
        cmds.delete(self.tract_polygons_to_remove)
