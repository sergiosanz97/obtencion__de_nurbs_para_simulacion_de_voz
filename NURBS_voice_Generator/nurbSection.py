# -*- coding: utf-8 -*-

import maya.cmds as cmds
import random
import re
import numpy as np
import math
from functools import partial
from OBB.api import OBB
import maya.mel as mel

class NurbSection(object):

    # Los nurbs mas cercanos a este
    first_shortesst_SinglePath = None
    second_shortesst_SinglePath = None


    def __init__(self):
        """Contructor del Nurb"""
        print("constructor")

    def setNameAndPosition(self, name, position):
        """Se especifiva el nombre del nurb y la piscion del pivote"""
        self.name = name
        self.position = position

    def getPosition(self):
        """Se devuelve la posicion del nurb"""
        return self.position

    def getScalarProduct(self, nurb1, nurb2):
        """A partir las distancias de los dos nurbs que se han 
        introducido y este nurb se calcula el producto escalr 
        """
        # Se obtienen las distancias de los que se han introducido como parametros
        nurb1_distance = nurb1.getPosition()- self.position
        nurb2_distance = nurb2.getPosition() - self.position

        # Se obtiene el vcetor normalizado de las distancias de los que se han introducido como parametros
        nurb1_norm = nurb1_distance / np.linalg.norm(nurb1_distance)
        nurb2_norm = nurb2_distance / np.linalg.norm(nurb2_distance)

        #Se calcula el producto escalar
        scalar_product = np.dot(nurb1_norm, nurb2_norm)
        return scalar_product

    def calculateDistance(self, nurb):
        """Se devuelve la magnitud de la distancia entre el nurb que se pasa como parametro este propio nurb"""
        return np.linalg.norm(nurb.getPosition() - self.position)
        
    def setBoundingBox(self, bounding_box):
        """Se asigna el nombre del objeto que es la bounding box de este nurb"""
        self.bounding_box = bounding_box

    def getBoundingBox(self):
        """Se devuelve el nombre del objeto que es la bounding box de este nurb"""
        return self.bounding_box

    def deleteBoundingBox(self):
        """Se borra la bounding box de este nurb"""
        cmds.delete(self.bounding_box)

    def isBBIntersected(self, aux_bb):
        """ Si la bounding box de este nurb y la bounding box(aux_bb)
        colisionan se devuelve True, en caso contarrio es False
        """
        # Sec calcula la interseccion entre las bounding boxes 
        op = cmds.polyCBoolOp(aux_bb, self.bounding_box ,n = "bbs_intersection", op = 3, ch = True, preserveColor = 0, classification = 1)
        cmds.select(op[0])
        vertices = cmds.polyEvaluate( v=True )
        cmds.undo()
        cmds.undo()
        # En el caso en que el numero de vertices de la interseccion
        # sea igual a 0, querra decir que no hay colision.
        if vertices == 0:
            return False
        else:
            return True

    def isBBcollision(self,name):
        """Se mira si la bounding box del propio nurb colisiona la de otro nurb, siempre y cuanto tengan la misma orientacion"""
        bb1 = cmds.xform(cmds.listRelatives(name, p = True)[0], q = 1, bb = 1)
        bb2 = cmds.xform(cmds.listRelatives(self.getName(), p = True)[0], q = 1, bb = 1)

        if not ( (bb1[0]>bb2[3]) or (bb2[0]>bb1[3])
            or (bb1[1]>bb2[4]) or (bb2[1]>bb1[4])
            or (bb1[2]>bb2[5]) or (bb2[2]>bb1[5]) ):
            return True
        else:
            return False

    def getName(self):
        """Devuelve el nombre del nurb"""
        return self.name
        
    def setDistance(self, dist):
        """Se asigna el valor de distancia"""
        self.distance = dist

    def getDistance(self):
        """Devuelve el valor del nurb"""
        return self.distance
        
    def getFirstNearestSinglePath(self):
        """Devuelve el nurb mas cercano"""
        return self.first_shortesst_SinglePath
    
    def getSeconNearestSinglePath(self):
        """Devuelve el segundo nurb mas cercano"""
        return self.second_shortesst_SinglePath
    
    def isInArray(self,nurbs = []):
        """Se compueaba que este nurb no este en el array de nurbs"""
        for nrb in nurbs:
            if nrb.getName() == self.name:
                return True
        return False
        
    def __repr__(self):
        """Representacion del Nurb"""
        if self.second_shortesst_SinglePath != None:
            return self.name + "  firstShorteset   " + self.first_shortesst_SinglePath.getName() + "    secondShorteset     "    + self.second_shortesst_SinglePath.getName()
        else:
            return self.name + "  firstShorteset   " + self.first_shortesst_SinglePath.getName() + "    secondShorteset: None"
             
    def searchNearestsPivots(self, nurbs = []):
        """Se buscan los nurbs mas cercanos"""
        # Se busca el nurb mas cercano del array de nurbs 
        for aux_nurb in nurbs:
            # En el caso de que el nurb del array sea este mismo no cuenta como mas cercano
            if aux_nurb.name == self.name: 
                continue 
            else:
                dist = self.calculateDistance(aux_nurb)
                
                # Si las bounding box de los propios nurbs no colisionan 
                # o las que estan generadas a partir de la malla de este nurb tampoco colisiona
                # Querra decir que no puede ser uno de los nurbs mas cercanos 
                if  self.isBBcollision(aux_nurb.name) == False or self.isBBIntersected(aux_nurb.getBoundingBox()) == False:
                    continue

                # Si la distancia de aux_nurb es menor que el mas cercano,  el aux_nurb pasara a ser el mas cercano 

                if self.first_shortesst_SinglePath == None:
                    self.first_shortesst_SinglePath = aux_nurb
                    self.first_shortesst_SinglePath.setDistance(dist)
                    
                else:
                    if dist < self.first_shortesst_SinglePath.getDistance():
                        self.first_shortesst_SinglePath =aux_nurb
                        self.first_shortesst_SinglePath.setDistance(dist)

        for aux_nurb in nurbs:
            # En el caso de que el nurb del array sea este mismo no cuenta como mas cercano
            if aux_nurb.name == self.name: 
                continue 
            else:
                dist = self.calculateDistance(aux_nurb)
                scalar = self.getScalarProduct(aux_nurb, self.first_shortesst_SinglePath)

                # Si las bounding box de los propios nurbs no colisionan 
                # o las que estan generadas a partir de la malla de este nurb tampoco colisiona
                # o el producto escalar entre las dos distancia entre el propio nurb son mayores que 0
                # Querra decir que no puede ser uno de los nurbs mas cercanos 
                if  self.isBBcollision(aux_nurb.name) == False or self.isBBIntersected(aux_nurb.getBoundingBox()) == False or aux_nurb.getName() == self.first_shortesst_SinglePath.getName() or scalar > 0:
                    continue

                # Si la distancia de aux_nurb es menor que el segundo mas cercano,  el aux_nurb pasara a ser el segundo mas cercano 

                if self.second_shortesst_SinglePath == None:
                    self.second_shortesst_SinglePath = aux_nurb
                    self.second_shortesst_SinglePath.setDistance(dist)
                    
                else:
                    if dist < self.second_shortesst_SinglePath.getDistance():
                        self.second_shortesst_SinglePath =aux_nurb
                        self.second_shortesst_SinglePath.setDistance(dist)
