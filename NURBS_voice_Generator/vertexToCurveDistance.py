# -*- coding: utf-8 -*-

import maya.cmds as cmds
import re
import numpy as np
import math
import maya.mel as mel


class VertexToCurveDistance(object):
    def __init__(self, curve_name, mesh_name, bocal_border_curve_name):
        """Constructor"""
        self.mesh_name = mesh_name
        self.distance = 10000000
        self.curve =  curve_name
        # Obtencion de la posicion del cv[0]
        bocal_edge_zero_position = cmds.pointPosition(bocal_border_curve_name + '.cv[0]' )
        self.bocal_edge_zero_position_vec = np.array([bocal_edge_zero_position[0], bocal_edge_zero_position[1], bocal_edge_zero_position[2]])

    def  addVertex(self, vertex_number):
        """ Se mira si el vertice es mas cercano que el que ya esta actualmente"""

        # Se gaurda la posicon del vertice
        postion = cmds.xform(self.mesh_name + '.vtx[' + str(vertex_number) + ']', q=True, ws=True, t=True)
        position_vector = np.array([postion[0], postion[1], postion[2]])
        
        # Se calcula la magnitud de la distancia entre el cv[0] y el vertice
        distance = np.linalg.norm(self.bocal_edge_zero_position_vec - position_vector)
        
        # Si la distancia es menor se guarda el vertice,
        # sera el vertice de partida para hacer el corte lateral
        if distance <= self.distance :
            self.vertex_number = vertex_number
            self.distance = distance

    def getVertexNumber(self):
        """Se devuelve el numero del vetice mas cercano al cv[0]"""
        return int(self.vertex_number)