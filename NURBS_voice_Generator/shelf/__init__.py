import os
from maya import mel
from maya import cmds

try:
    from PySide import QtGui
except ImportError:
    from PySide2 import QtGui

from functools import partial
from collections import OrderedDict

this_package = os.path.abspath(os.path.dirname(__file__))
shelf_path = partial(os.path.join, this_package)

buttons = OrderedDict({
    'Tracto y Labios': {
        'command': (
            "#!/usr/bin/python\n"
            "# -*- coding: iso-8859-15 -*-\n"
            "import NURBS_voice_Generator.ventanaWithLips\n"
            "v =  NURBS_voice_Generator.ventanaWithLips.Ventana()"
        ),
        'sourceType': 'python',
        'style': 'iconOnly',
        'image': shelf_path('tracto_y_labios.png'),
        'annotation': 'AOV Matte management',
        'enableCommandRepeat': False,
        'flat': True,
        'enableBackground': False,
    },
    'Tracto': {
        'command': (
            "#!/usr/bin/python\n"
            "# -*- coding: iso-8859-15 -*-\n"
            "import NURBS_voice_Generator.ventana\n"
            "v =  NURBS_voice_Generator.ventana.Ventana()"
        ),
        'sourceType': 'python',
        'style': 'iconOnly',
        'image': shelf_path('tracto.png'),
        'annotation': 'AOV Matte management',
        'enableCommandRepeat': False,
        'flat': True,
        'enableBackground': False,
    }
})


def create_shelf():
    """
    Create the Generacion_NURBS_para_simulacion_de_voz shelf

    Raises:
        None

    Returns:
        None
    """

    tab_layout = mel.eval('$pytmp=$gShelfTopLevel')
    shelf_exists = cmds.shelfLayout('GeneracionDeVozNURBS', exists=True)

    if shelf_exists:
        cmds.deleteUI('GeneracionDeVozNURBS', layout=True)

    shelf = cmds.shelfLayout('GeneracionDeVozNURBS', parent=tab_layout)

    for button, kwargs in buttons.items():

        img = QtGui.QImage(kwargs['image'])
        kwargs['width'] = img.width()
        kwargs['height'] = img.height()

        cmds.shelfButton(label=button, parent=shelf, **kwargs)

    # Fix object 0 error.
    shelves = cmds.shelfTabLayout(tab_layout, query=True, tabLabelIndex=True)

    for index, shelf in enumerate(shelves):
        cmds.optionVar(stringValue=("shelfName%d" % (index+1), str(shelf)))

create_shelf()
