# -*- coding: utf-8 -*-

import maya.cmds as cmds
from functools import partial

import morpherAdapter
import meshAdapterWithLips
import nurbGeneratorWithLips
import utils 
import fileExporterWithLips

class Ventana(object):


    def AdaptMesh(self, *args):
        """ Se unen todos los objetos del tracto en uno"""
        self.final_mesh_name = cmds.textField(self.name,  q=True, text=True)

        # Se setea el numero de secciones a borrar del tracto vocal
        self.meshAdapter.setNumberSectionToRemoveMouth(cmds.intSliderGrp(self.sectionsTractToRemove,q=True,v=True))

        # se unen todos los objetos que forman parte del tracto vocal y se unen en uno.
        # Tmbien se unen los vetrivces que estan en la misma posición
        self.meshAdapter.AdaptMesh(self.final_mesh_name)
        print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        # y se obtienen los edges del borde de la boca que conectan con el tracto
        self.meshAdapter.getFaceCurve()
        print("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
        # Se borran tos triangulos del extremo del tracto vocal y se obtene el borde de edges que conecta con la boca
        self.meshAdapter.removeAllTriangles()


    def setMorpher(self, *args):
        """Se modifican los morphers seleccionados para que el area 
        se adapte correctamente al area del borde extremo del tracto
        """
        face_name = self.fm.getMouthName()

        self.morpherAdapter = morpherAdapter.MorpherAdapter()
        self.morpherAdapter.setFaceName(face_name)

        # Todos Los morphers correspondiengtes a bocales de la boca se ponen a 0
        self.morpherAdapter.setToZeroMorpherValues()

        # se mira que caillas de los morpherers estan seleccionadas y se aplica el gradiante descendeiente a cada una de las casillas seleccionadas
        if cmds.checkBox(self.ai, q = True, v= True) == True:
            self.morpherAdapter.gradDescMorpher( "AI"  ,self.meshAdapter.getFaceBorder(), self.meshAdapter.getBocalEdge() )

        if cmds.checkBox(self.e, q = True, v= True) == True:
            self.morpherAdapter.gradDescMorpher( "E"  ,self.meshAdapter.getFaceBorder(), self.meshAdapter.getBocalEdge() )

        if cmds.checkBox(self.o, q = True, v= True) == True:
            self.morpherAdapter.gradDescMorpher( "O"  ,self.meshAdapter.getFaceBorder(), self.meshAdapter.getBocalEdge() )

        if cmds.checkBox(self.u, q = True, v= True) == True:
            self.morpherAdapter.gradDescMorpher( "U"  ,self.meshAdapter.getFaceBorder(), self.meshAdapter.getBocalEdge() )


    def moveTract(self, *args):
        """Colocar tracto vocal en la boca/cara"""
        #se mueve el tracto bocal a la posicion que corresponde tieneindo en cuenta los labios y las dimensiones de la apertura del tracto vocal
        face_name = self.fm.getMouthName()
        is_full_face = self.fm.getFullFace()
        self.morpherAdapter.alignLipsAndTract(self.meshAdapter.getName(),
            self.meshAdapter.getBocalEdge(),
            self.meshAdapter.getFaceBorder() , 
            face_name,
            is_full_face
            )
        
    def convertFaceToBurb(self, *args):
        """ Se hace la seleccion de los poligonos del extremo a borrar
         de la boca y de los edges del nuevo borde
        """
        # Obtención de la borde de ls boca teniendo en cuenta los poligonos 
        # y tambien se obtienen estos poligonso 
        cmds.select(self.meshAdapter.getBorderFaceEdge())
        mouth_polygons_to_remove, mouth_extreme_border = self.meshAdapter.removeSections(
            border_edge = self.meshAdapter.getBorderFaceEdge(),
            section_number=  cmds.intSliderGrp(self.sectionsMouthToRemove,q=True,v=True)
            )
        self.meshAdapter.setMouthExtremeBorder(mouth_extreme_border)
        self.meshAdapter.setMouthPolygonsToRemove( mouth_polygons_to_remove)

        is_full_face = self.fm.getFullFace()
        # Coviersion de la boca a nurbs
        self.nurbsGenerator.convertBocalFaceToNurb(self.meshAdapter.getMouthPolygonsToRemove(), is_full_face)

    def makeSections(self, *args):
        """ Se seccionan la maya en secciones """
        # Seccionamiento del tracto vocal
        self.meshAdapter.setBocalBorderCurve(self.nurbsGenerator.getbocalBorderCurve())

        self.meshAdapter.makeSections()


    def makeUnion(self, *args):
        """ Se crea la union entre el tracto y la boca/cara"""
        # Se hace la union entre los bordes de la boca y del tracto para crear un nurb
        lofted_name = cmds.loft( 
            self.nurbsGenerator.getbocalBorderCurve(), self.meshAdapter.getBorderCurve(),
            ch=True, r=True, u = 1 
            )[0]
        self.nurbsGenerator.setLoftedName(lofted_name)

    def GenerateNurbs(self, *args):
        """ Se converte la maya seccioda a secciones en nurbs """
        #Se convierte el tracto a nurbs y se se guarda en un array el orden correcto de las secciones en nurbs
        group_size =  cmds.intSliderGrp(self.seccionsOfGroup ,q=True,v=True)
        self.nurbsGenerator.makeNurbs(self.meshAdapter.getName())

    def mergeNurbs(self,*args):
        """ Se hace el merge de los nurbs de las secciones """
        reverse =  cmds.checkBox (self.reverse, q = True, value = True)
        group_size =  cmds.intSliderGrp(self.seccionsOfGroup ,q=True,v=True)
        self.nurbsGenerator.setReverseOrder(reverse)

        # Se especifican las caractericrcas del rebuild y se hace la union de los nurbs
        self.nurbsGenerator.attachNurbs(group_size, 1)


    def importMouth(self,*args):
        """ Importacion de la boca"""
        #importación de la boca
        self.fm.importMouth()
        mouth_name = self.fm.getMouthName()
        self.nurbsGenerator.setFaceName(mouth_name)
        self.meshAdapter.setFaceName(mouth_name)


    def importFace(self,*args):
        """ Importacion de la cara entera"""
        # importación del la cara
        self.fm.importFace()
        face_name = self.fm.getMouthName()
        self.nurbsGenerator.setFaceName(face_name)
        self.meshAdapter.setFaceName(face_name)

    def importBocalTract(self,*args):
        """ Importacion del tracto vocal"""
        # Indicacion de los posibles formatos de los archicvos de tractos vocal
        basicFilter = "*.igs *.iges *.dxf"

        #obtención del path del archivo
        fPath =cmds.fileDialog2(fileFilter=basicFilter, dialogStyle=2, fileMode = 1)
        
        # Importacion del tracto vocal
        tract_objs = self.fm.importTract(path = fPath)
        self.meshAdapter.setTractObjs(tract_objs)

    def exportNurbs(self, *args):
        """ Exportacion de los objetos del tacto con la union y la boca"""
        # Se abre un exporador de archivos para seleccionar el directiorio done guardar el archivo
        fPath =cmds.fileDialog2( dialogStyle=2, fileMode = 3)

        # En el caso de que el usuario haya cancelado el paso anterior se cancela el proceso de exportar
        if fPath == None:
            return

        # Se seleccionan unicamente los nubs que contienen el tracto vocal con la union y la boca
        cmds.select(cl = True)
        self.nurbsGenerator.selectTractAndUnion()
        self.nurbsGenerator.selectMouth()
        file_name= cmds.textField(self.name,  q=True, text=True)

        fPath = fPath[0] + "/" + file_name

        cmds.file(fPath,  es = True, type = "IGESExport")

    def __init__(self):
        """ Crea la ventana"""
        # Se crean las clase
        self.nurbsGenerator = nurbGeneratorWithLips.NurbGenerator()
        self.fm = fileExporterWithLips.FileManager()
        self.meshAdapter = meshAdapterWithLips.MeshAdapter()

        nombre_de_ventana = 'conversion_del_tracto_y_labios'
        
        # se crea la ventana. en el caso de que ya exista, se cierra la antigua
        if cmds.window(nombre_de_ventana, exists = True):
            cmds.deleteUI(nombre_de_ventana)
        ventana = cmds.window(nombre_de_ventana, resizeToFitChildren=True, width = 400)

        #Creacion de las dimensiones de la ventana y el estilo
        cmds.columnLayout(columnAttach=('both', 10), rs = 7, columnWidth=400)

        #creación de los botones para importar
        cmds.button('Importar boca', command = self.importMouth)
        cmds.button('Importar cara', command = self.importFace)
        cmds.button('Importar tracto vocal', command = self.importBocalTract)        
        
        # Asignacion del nombre del nurb del tracto bocal
        # Este mismo nombre sera el del arxivo cuando se exporte
        cmds.text("Alinee correctamente el tracto vocal", wordWrap = True)
        cmds.rowLayout(numberOfColumns=2, adjustableColumn=1,columnWidth2=[250, 150])
        name = cmds.text("Nombre del archivo a exportar: " )
        self.name = cmds.textField(ed = True, width = 150, tx = "Nombre")
        cmds.setParent('..')
        
        # Slider para indicar el numero de secciones de poligonos a borrar en el extremo del tracto 
        cmds.rowLayout(numberOfColumns=2,columnWidth2=[100, 300])
        cmds.text("Numero de secciones del tracto a borrar:", wordWrap = True, width = 200, )
        self.sectionsTractToRemove = cmds.intSliderGrp(v = 1, min = 0, max = 3, 
            width = 150, s=1, field = True, cw2= [20,60]
            )
        cmds.setParent('..')

        # Slider para indicar el numero de secciones de poligonos a borrar en el extremo de la boca
        cmds.rowLayout(numberOfColumns=2,columnWidth2=[100, 250])
        cmds.text("Numero de secciones de la boca a borrar:", wordWrap = True, width = 200)
        self.sectionsMouthToRemove = cmds.intSliderGrp(v = 1, min = 0, max = 3, 
            width = 150, s=1, field = True,cw2= [20,60] 
            )
        cmds.setParent('..')

        # Boton pa hacer el merge de todos los objetos que forman el tracto vocal
        cmds.button('Unir todos los objetos del tracto en uno', command = partial(self.AdaptMesh))
        
        # Checkbox que contienen la informacio de a que morphers hay que aplicarle el tracto vocal
        cmds.rowLayout(numberOfColumns=4, adjustableColumn=1,columnWidth4=[100, 100, 100, 100])
        self.ai = cmds.checkBox( label='AI', value = False )
        self.e = cmds.checkBox( label='E', value = False )
        self.o = cmds.checkBox( label='O', value = False )
        self.u = cmds.checkBox( label='U', value = False )
        cmds.setParent('..')

        # Boton para haver el gradiante descendiente a los morphers.
        cmds.button('Ajustar morpher', command = partial(self.setMorpher))

        # boton para posicionar el tracto vocal correctamente respecto a la boca. 
        cmds.button('Posicionar tracto vocal', command = partial(self.moveTract))

        # Boton para seleccionar los poligonos necesario que hay que borrar en la boca.
        # Tambien se gurada el nuevo ectremo real de la boca teniendo en cuenta los poligonos eliminados
        # y Se convierten los poligonos de la boca a nurbs
        cmds.button('Convertir boca a nurbs', command = partial(self.convertFaceToBurb))

        # Se secciona el tracto vocal y se eliminan las secciones del extremo del tracto que se hayan indicado anteriormente.
        cmds.button('Seccionar tracto vocal', command = partial(self.makeSections))

        # Se crea un nurb de union entre el tracto vocal y la boca
        cmds.button('Crear union entre boca y tracto', command = partial(self.makeUnion))
        
        # Indica el Metodo a utilizar para unir los nurbs
        cmds.text("Introduce los parametros para generar el tracto vocal en nurbs.", wordWrap = True)
        
        # Seleccion del numero de nurbs que unir en uno solo.
        cmds.rowLayout(numberOfColumns=2,columnWidth2=[100, 300])
        cmds.text("Agrupar secciones en un grupos de:", wordWrap = True, width = 100)
        self.seccionsOfGroup = cmds.intSliderGrp(v = 2, min = 1, max = 6, width = 280, s=1, field = True )
        cmds.setParent('..')

        # Boton para convertir los poligonos del tracto a Nurbs.
        # Una vez convertido se crea un array para saber el orden de los nurbs
        cmds.button('Generar Nurb/s', command = partial(self.GenerateNurbs))
        # Checkbox para invertir el orden de la union
        self.reverse = cmds.checkBox (l = "Reverse direction", value = False)
        # Boton para unir todos los nurbs
        cmds.button('Unir Nurbs', command = partial(self.mergeNurbs))

        # Boron para exportar el nurb del tracto con la union y la boca
        cmds.button('Export', command = partial(self.exportNurbs))

        # Se muestra la ventana
        cmds.showWindow(nombre_de_ventana)