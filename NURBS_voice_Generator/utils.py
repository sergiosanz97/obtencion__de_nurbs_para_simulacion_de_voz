# -*- coding: utf-8 -*-

import maya.cmds as cmds
import numpy as np
import maya.mel as mel

def bakeCustomToolPivot(pos=1, ori=1):
    """BakePivot"""
    # Check 1) must have an object(s) selected
    objects = cmds.ls(sl=1, transforms=1)
    shapes = cmds.ls(sl=1, shapes=1)
    if len(shapes) > 0:
        transforms = cmds.listRelatives(path=1, parent=1, type='transform')
        objects += transforms

    #if len(objects) == 0:
    #    cmds.error("m_bakeCustomToolPivot.kNoObjectsSelectedError")
    #    return None

    # Check 2) must be in the move/rotate/scale tool
    currentCtx = cmds.currentCtx()
    contextList = ["moveSuperContext", "manipMoveContext", "RotateSuperContext",
                   "manipRotateContext", "scaleSuperContext", "manipScaleContext"]


    #if currentCtx not in contextList:
    #    cmds.error("m_bakeCustomToolPivot.kWrongToolError")
    #    return None

    # Check 3) must be in custom orientation mode
    customOri = []
    pivotModeActive = 0
    customModeActive = 0
    if currentCtx == "moveSuperContext" or currentCtx == "manipMoveContext":
        customOri = cmds.manipMoveContext('Move', q=1, orientAxes=1)
        pivotModeActive = cmds.manipMoveContext('Move', q=1, editPivotMode=1)
        customModeActive = cmds.manipMoveContext('Move', q=1, mode=1) / 6
    elif currentCtx == "RotateSuperContext" or currentCtx == "manipRotateContext":
        customOri = cmds.manipRotateContext('Rotate', q=1, orientAxes=1)
        pivotModeActive = cmds.manipRotateContext('Rotate', q=1, editPivotMode=1)
        customModeActive = cmds.manipRotateContext('Rotate', q=1, mode=1) / 3
    elif currentCtx == "scaleSuperContext" or currentCtx == "manipScaleContext":
        customOri = cmds.manipScaleContext('Scale', q=1, orientAxes=1)
        pivotModeActive = cmds.manipScaleContext('Scale', q=1, editPivotMode=1)
        customModeActive = cmds.manipScaleContext('Scale', q=1, mode=1) / 6

    #if ori and not pos and not customModeActive:
    #    cmds.error("m_bakeCustomToolPivot.kWrongAxisOriModeError")
    #    return None

    # Get custom orientation
    if ori and customModeActive:
        customOri[0] = mel.eval('rad_to_deg({})'.format(customOri[0]))
        customOri[1] = mel.eval('rad_to_deg({})'.format(customOri[1]))
        customOri[2] = mel.eval('rad_to_deg({})'.format(customOri[2]))
        # Set object(s) rotation to the custom one (preserving child transform positions and geometry positions)
        cmds.rotate(customOri[0], customOri[1], customOri[2], objects, a=1, pcp=1, pgp=1, ws=1, fo=1)

    if pos:
        for object in objects:
            # Get pivot in parent space
            # object = 'pSphere4'            
            old = [0, 0, 0]
            m = cmds.xform(object, q=1, m=1)
            p = cmds.xform(object, q=1, os=1, sp=1)
            old[0] = (p[0] * m[0] + p[1] * m[4] + p[2] * m[8] + m[12])
            old[1] = (p[0] * m[1] + p[1] * m[5] + p[2] * m[9] + m[13])
            old[2] = (p[0] * m[2] + p[1] * m[6] + p[2] * m[10] + m[14])

            # Zero out pivots
            cmds.xform(objects, zeroTransformPivots=1)

            # Translate object(s) back to previous pivot (preserving child transform positions and geometry positions)
            new = cmds.getAttr(object + ".translate")[0]
            cmds.move((old[0] - new[0]), (old[1] - new[1]), (old[2] - new[2]), object, pcp=1, pgp=1, ls=1, r=1)

    # Exit pivot mode
    if pivotModeActive:
        mel.eval('ctxEditMode;')

    # Set the axis orientation mode back to object
    if ori and customModeActive:
        if currentCtx == "moveSuperContext" or currentCtx == "manipMoveContext":
            cmds.manipMoveContext('Move', e=1, mode=0)
        elif currentCtx == "RotateSuperContext" or currentCtx == "manipRotateContext":
            cmds.manipRotateContext('Rotate', e=True, mode=0)
        elif currentCtx == "scaleSuperContext" or currentCtx == "manipScaleContext":
            cmds.manipScaleContext('Scale', e=1, mode=0)


def is_number_tryexcept(s):
    """ Returns True is string is a number. """
    try:
        int(s)
        return True
    except ValueError:
        return False
